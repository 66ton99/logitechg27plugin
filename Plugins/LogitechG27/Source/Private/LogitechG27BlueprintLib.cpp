#include "LogitechG27PluginPrivatePCH.h"
#include "LogitechG27BlueprintLib.h"

#define G27 (ILogitechG27Plugin::Get().GetDevice()._g27)
#define Device ILogitechG27Plugin::Get().GetDevice()

void ULogitechG27BlueprintLib::SetProperties(bool useForceFeedback, int32 overallGain, int32 springGain, int32 damperGain, bool combinePedals, int32 wheelRange, bool enableDefaultSpring)
{
	if (Device.IsDeviceAvailable())
		G27.SetProperties(useForceFeedback, overallGain, springGain, damperGain, combinePedals, wheelRange, enableDefaultSpring);
}

void ULogitechG27BlueprintLib::PlayLEDS(int32 minRpm, int32 maxRpm, int32 currentRpm)
{
	if (Device.IsDeviceAvailable())
	G27.PlayLEDS(minRpm, maxRpm, currentRpm);
}

void ULogitechG27BlueprintLib::PlayBumpyRoadEffect(int32 magnitude)
{
	if (Device.IsDeviceAvailable())
		G27.PlayBumpyRoadEffect(magnitude);
}

void ULogitechG27BlueprintLib::StopBumpyRoadEffect()
{
	if (Device.IsDeviceAvailable())
		G27.StopBumpyRoadEffect();
}

void ULogitechG27BlueprintLib::PlayCarAirborne()
{
	if (Device.IsDeviceAvailable())
		G27.PlayCarAirborne();
}

void ULogitechG27BlueprintLib::StopCarAirborne()
{
	if (Device.IsDeviceAvailable())
		G27.StopCarAirborne();
}

void ULogitechG27BlueprintLib::PlayConstantForce(int32 magnitudePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlayConstantForce(magnitudePercentage);
}

void ULogitechG27BlueprintLib::StopConstantForce()
{
	if (Device.IsDeviceAvailable())
		G27.StopConstantForce();
}

void ULogitechG27BlueprintLib::PlayDamperForce(int32 coefficientPercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlayDamperForce(coefficientPercentage);
}

void ULogitechG27BlueprintLib::StopDamperForce()
{
	if (Device.IsDeviceAvailable())
		G27.StopDamperForce();
}

void ULogitechG27BlueprintLib::PlayDirtRoadEffect(int32 magnitudePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlayDirtRoadEffect(magnitudePercentage);
}

void ULogitechG27BlueprintLib::StopDirtRoadEffect()
{
	if (Device.IsDeviceAvailable())
		G27.StopDirtRoadEffect();
}

void ULogitechG27BlueprintLib::PlayFrontalCollisionForce(int32 magnitudePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlayFrontalCollisionForce(magnitudePercentage);
}

void ULogitechG27BlueprintLib::PlaySideCollisionForce(int32 magnitudePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlaySideCollisionForce(magnitudePercentage);
}

void ULogitechG27BlueprintLib::PlaySlipperyRoadEffect(int32 magnitudePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlaySlipperyRoadEffect(magnitudePercentage);
}

void ULogitechG27BlueprintLib::StopSlipperyRoadEffect()
{
	if (Device.IsDeviceAvailable())
		G27.StopSlipperyRoadEffect();
}

void ULogitechG27BlueprintLib::PlaySoftstopForce(int32 usableRangePercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlaySoftstopForce(usableRangePercentage);
}

void ULogitechG27BlueprintLib::StopSoftstopForce()
{
	if (Device.IsDeviceAvailable())
		G27.StopSoftstopForce();
}

void ULogitechG27BlueprintLib::PlaySpringForce(int32 offsetPercentage, int32 saturationPercentage, int32 coefficientPercentage)
{
	if (Device.IsDeviceAvailable())
		G27.PlaySpringForce(offsetPercentage, saturationPercentage, coefficientPercentage);
}

void ULogitechG27BlueprintLib::StopSpringForce()
{
	if (Device.IsDeviceAvailable())
		G27.StopSpringForce();
}

void ULogitechG27BlueprintLib::PlaySurfaceEffect(int32 periodicType, int32 magnitude, int32 frequency)
{
	if (Device.IsDeviceAvailable())
		G27.PlaySurfaceEffect(periodicType, magnitude, frequency);
}

void ULogitechG27BlueprintLib::StopSurfaceEffect()
{
	if (Device.IsDeviceAvailable())
		G27.StopSurfaceEffect();
}